#include "service_view_impl.h"
#include "service_ctrl_impl.h"

ServiceViewImpl::ServiceViewImpl(void)
{
	srcCtrl = new ServiceCtrlImpl;
}
	
ServiceViewImpl::~ServiceViewImpl(void)
{
	delete srcCtrl;
}
	
void ServiceViewImpl::menuSrc(void)
{
	while(true)
	{
		clear_screen();
		info("******Welcome to the operating subsystem******\n");
		info("		1、Add department\n");
		info("		2、Delete department\n");
		info("		3、List all department\n");
		info("		4、Add employee\n");
		info("		5、Delete employee\n");
		info("		6、Modify employee's information\n");
		info("		7、List all employees in the department\n");
		info("		8、List all employees\n");
		info("		0、Exit\n");
		
		switch(get_cmd('0','8'))
		{
			case '1': addDept(); break;
			case '2': delDept(); break;
			case '3': listDept(); break;
			case '4': addEmp(); break;
			case '5': delEmp(); break;
			case '6': modEmp(); break;
			case '7': listEmp(); break;
			case '8': listAllEmp(); break;
			case '0': return;
		}
	}
}

void ServiceViewImpl::addDept(void)
{
	char name[20] = {};
	info("Please input department name：");
	get_str(name,sizeof(name));
	
	Department dept(name);
	if(srcCtrl->addDept(dept))
	{
		info("Add department successful！",1.5);
	}
	else
	{
		info("Fail to add department！",1.5);
	}
}

void ServiceViewImpl::delDept(void)
{
	int id;
	info("Please input department ID for deletion:");
	cin >> id;
	
	if(srcCtrl->delDept(id))
	{
		info("Delete department successcul！",1.5);
	}
	else
	{
		info("Fail to delete department！",1.5);
	}
}

void ServiceViewImpl::listDept(void)
{
	vector<Department> deptArr = srcCtrl->listDept();
	
	for(uint32_t i=0; i<deptArr.size(); i++)
	{
		cout << deptArr[i].getId() << " " << deptArr[i].getName() << " " << deptArr[i].empArr.size() << endl;
	}
	
	anykey_continue();
}

void ServiceViewImpl::addEmp(void)
{
	int id;
	char name[20] = {};
	char sex = 0;
	int age = 1;
	
	info("Please input department ID to join：");
	cin >> id;
	
	info("Please input employee name：");
	get_str(name,sizeof(name));
	
	info("Please input employee sex：");
	sex = get_sex();
	
	info("Please input employee age：");
	cin >> age;
	
	Employee emp(name,sex,age);
	if(srcCtrl->addEmp(id,emp))
	{
		info("Add employee successful！",1.5);
	}
	else
	{
		info("Fail to add employee！",1.5);
	}
}

void ServiceViewImpl::delEmp(void)
{
	int id;
	info("Please input employee ID for deletion：");
	cin >> id;
	
	if(srcCtrl->delEmp(id))
	{
		info("Delete employrr successful！",1.5);
	}
	else
	{
		info("Fail to delete employee！",1.5);
	}
}

void ServiceViewImpl::modEmp(void)
{
	int id;
	char name[20] = {};
	char sex = 0;
	int age = 1;
	
	info("Please input employee ID to modify：");
	cin >> id;
	
	info("Please input employee name：");
	get_str(name,sizeof(name));
	
	info("Please input employee sex：");
	sex = get_sex();
	
	info("Please input employee age：");
	cin >> age;
	
	Employee emp(name,sex,age,id);
	if(srcCtrl->modEmp(id,emp))
	{
		info("Modify employee information！",1.5);
	}
	else
	{
		info("Fail to modify employee information！",1.5);
	}
}

void ServiceViewImpl::listEmp(void)
{
	int id;
	info("Please input department ID to list:");
	cin >> id;
	
	Department* dept = srcCtrl->listEmp(id);
	if(NULL == dept)
	{
		info("Department isn't exist！",1.5);
		return;
	}
	
	cout << dept->getId() << " " << dept->getName() << " " << dept->empArr.size() << endl;
	
	for(uint32_t i=0; i<dept->empArr.size(); i++)
	{
		cout << dept->empArr[i] << endl;
	}
	
	anykey_continue();
}

void ServiceViewImpl::listAllEmp(void)
{
	vector<Department>& deptArr = srcCtrl->listDept();
	
	for(uint32_t i=0; i<deptArr.size(); i++)
	{
		cout << deptArr[i] << endl;
	}
	
	anykey_continue();
}
