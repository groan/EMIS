#include "manager_view_impl.h"
#include "emis.h"
#include "tools.h"

EMIS* EMIS::emis;

EMIS::EMIS(void)
{
	init_id();
	mgrView = new ManagerViewImpl;
}

EMIS::~EMIS(void)
{
	delete mgrView;
}

EMIS* EMIS::create(void)
{
	if(NULL == emis)
	{
		emis = new EMIS;
	}
	return emis;
}

void EMIS::start(void)
{
	while(true)
	{
		clear_screen();
		
		info("******Welcome to GROAN enterprise information management system******\n");
		info("		1、Login management subsystem\n");
		info("		2、Login operation subsystem\n");
		info("		0、Exit\n");
		
		switch(get_cmd('0','2'))
		{
			case '1': mgrView->loginManager(); break;
			case '2': mgrView->loginService(); break;
			case '0': delete emis; return;
		}
	}
}
